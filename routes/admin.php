<?php

Route::group(['prefix' => 'inicio', 'as' => 'inicio.'], function() {
    Route::get('/', ['as' => 'index', 'uses' => 'InicioController@index']);
    
});



Route::group(['prefix' => 'nosotros', 'as' => 'nosotros.'], function() {
    Route::get('/', ['as' => 'index', 'uses' => 'NosotrosController@index']);

    
});