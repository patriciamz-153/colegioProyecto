@extends('layouts.admin')

@section('content')

    

    <div class="row" id="app">
        <div class="panel panel-info">
            <div class="panel-heading text-center">
                <h3>Inicio</h3>
            </div>
            <div class="panel-body">
                <div class="panel-heading text-center" id="column"><!-- column -->
                    <div class="panel-body" id="content"><!-- content -->
                      <h3>Qué es Maranguita</h3>                      
                      <p><span id="quees">Maranguita es un Movimiento de Educación Popular Integral y Promoción Social,  basado en los valores de justicia, libertad, participación y fraternidad, dirigida a la población empobrecida y excluida para contribuir a la transformación de las sociedades.</span></p>
                      <p>&nbsp;</p>

                      <h2>Misión</h2>
                      <p class="lead" >Brindar una educación pública de calidad desde los valores evangélicos, con el propósito de contribuir a la formación de ciudadanos capaces de mejorar sus condiciones de vida y la transformación de las sociedades.</p>
                      <p>&nbsp;</p>
                      <h3>Visión</h3>
                    </div>
                    <div class="panel-body" id="vision"><!-- group -->
                        
                           <p>Ser referente válido para la educación pública.</p>
                           <p>Implementar una propuesta educativa sistematizada que responda a las necesidades de los excluidos.</p>
                           <p>Formar ciudadanos éticos, democráticos y comprometidos con la transformación de la sociedad y el desarrollo sostenible.</p>
                        
                    </div>    
                </div>
            </div>    
            
        </div>
    </div>
@endsection
